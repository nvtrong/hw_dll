//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 8.1App.rc
//
#define IDC_MYICON                      2
#define IDD_MY81APP_DIALOG              102
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MY81APP                     107
#define IDI_SMALL                       108
#define IDC_MY81APP                     109
#define IDD_ABOUTBOX                    110
#define IDR_MAINFRAME                   128
#define IDD_DLL                         129
#define IDC_GT1                         1000
#define IDC_GT2                         1001
#define ID_ADD                          1002
#define ID_SUB                          1003
#define ID_MUL                          1004
#define ID_DEV                          1005
#define ID_RESULT                       1007
#define ID_QUIT                         1008
#define ID_DLL                          32772
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
