// H8.1.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <Windowsx.h>


#define EXPORT _declspec(dllexport)

int a, b;

EXPORT int add(int a, int b)
{
	return (a + b);
}
EXPORT int sub(int a, int b)
{
	return (a - b);
}

EXPORT int mul(int a, int b)
{
	return (a * b);
}

EXPORT int dev(int a, int b)
{
	return (a / b);
}